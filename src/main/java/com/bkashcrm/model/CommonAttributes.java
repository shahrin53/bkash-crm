package com.bkashcrm.model;

import javax.persistence.*;

@Entity
@Table(name="common_attributes")

public class CommonAttributes{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="attribute_name")
	private String attribute_name;
	
	@Column(name="module_name")
	private String module_name;

	public String getAttribute_name() {
		return attribute_name;
	}

	public void setAttribute_name(String attribute_name) {
		this.attribute_name = attribute_name;
	}

	public String getModule_name() {
		return module_name;
	}

	public void setModule_name(String module_name) {
		this.module_name = module_name;
	}

	
	
}

