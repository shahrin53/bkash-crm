package com.bkashcrm.dao;
import java.time.LocalDateTime;
import java.util.*;

import com.bkashcrm.model.TimerSettings;
import com.bkashcrm.util.AppConstant;
import com.bkashcrm.util.HibernateUtil;
import com.bkashcrm.util.TimeConversion;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;

public class SupportEmailSchedulerDAO implements TimerSettingsDAO {
	
	static final Logger logger = Logger.getLogger(SupportEmailSchedulerDAO.class.getName());
		
	String hql;
	Query query;  

   
	public List<TimerSettings> getParameters(String module) {
		// TODO Auto-generated method stub
		Session session =null;
		List<TimerSettings> paramList = null;
		
		try
		{
			SessionFactory sessionFactory = HibernateUtil.sessionFactory;
            session = sessionFactory.openSession();
			
			hql = "From TimerSettings where module=:module_name";
			query = session.createQuery(hql);
			query.setParameter("module_name", module);
			
			paramList = query.getResultList();
			
			//session.close();
		}
		catch (Exception e) {
            e.printStackTrace();
        } finally {
        	session.close();
           
        }
		
		return paramList;
	}
	
	
	public void scheduleJobToGetSupportMail(List<TimerSettings> emailJobParamList)
	{
		String paramName;
		int delay=0,period=0;
		String timeUnit;
		long timeinMillis =0;
		
		try
		{
		
			for(TimerSettings param : emailJobParamList){
				paramName = param.getParameter();
				timeUnit = param.getUnit();	
				
				
				if(paramName!=null && paramName.equals(AppConstant.read_support_email_period))
				{
					period = param.getParamValue();
					timeinMillis = TimeConversion.TimeToLong(period,timeUnit);
					System.out.println("period : "+timeinMillis);
					logger.debug("period : "+timeinMillis);
					
				}
				else if(paramName!=null && paramName.equals(AppConstant.read_support_email_delay))
				{
					delay = param.getParamValue();
					timeinMillis = TimeConversion.TimeToLong(delay,timeUnit);
					System.out.println("delay  : "+timeinMillis);
					logger.debug("delay  : "+timeinMillis);
				}
				
			}
			
			Timer timer = new Timer();
			
			
			timer.schedule(new TimerTask() {
	
				
	            public void run() {
	            	
	            	System.out.println("Hellow - reading bKash Problem Email "+LocalDateTime.now());
	            	new EmailReceiverDAO().getUnreadEmail();
	            	
	            }
	        }, delay, period);
			
			
		}
		
		
		catch(Exception e)
		{
			logger.debug(e);
		}
		
	}

}