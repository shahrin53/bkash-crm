package com.bkashcrm.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.bkashcrm.dto.EmailAttachmentDTO;
import com.bkashcrm.dto.EmailDTO;
import com.bkashcrm.dto.EmailMsgDTO;

public interface EmailStoreDAO {	
	
	public void setEmailInfo(ArrayList<EmailDTO> dtos,HashMap<Integer,ArrayList<EmailAttachmentDTO>> attachmentsMap);
	public void setEmailMessage(EmailMsgDTO mailMsgDto);
	public void setMailAttachment(int mail_id,ArrayList<EmailAttachmentDTO> attachmentList);
	
}