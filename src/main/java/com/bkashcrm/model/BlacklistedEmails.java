package com.bkashcrm.model;

import javax.persistence.*;

@Entity
@Table(name="blacklisted_emails")

public class BlacklistedEmails{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="email")
	private String email;
	
	@Column(name="created_on")
	private long created_on;

	public long getCreated_on() {
		return created_on;
	}

	public void setCreated_on(long created_on) {
		this.created_on = created_on;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}

