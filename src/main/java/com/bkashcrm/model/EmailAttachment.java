package com.bkashcrm.model;

import javax.persistence.*;

@Entity
@Table(name="email_attachment")

public class EmailAttachment{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="mail_id")
	private int mail_id;
	
	public int getMail_id() {
		return mail_id;
	}

	public void setMail_id(int mail_id) {
		this.mail_id = mail_id;
	}

	public String getAttached_filename() {
		return attached_filename;
	}

	public void setAttached_filename(String attached_filename) {
		this.attached_filename = attached_filename;
	}

	public String getAtcched_filename_original() {
		return atcched_filename_original;
	}

	public void setAtcched_filename_original(String atcched_filename_original) {
		this.atcched_filename_original = atcched_filename_original;
	}

	@Column(name="attached_filename")
	private String attached_filename;
	
	@Column(name="atcched_filename_original")
	private String atcched_filename_original;

	
	
}

