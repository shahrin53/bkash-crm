package com.bkashcrm.dto;

import javax.mail.Address;

public class EmailDTO {
	
	private String domainName;	
	private String fromEmail;
	private String toEmail;
	private String subject;
	private Address[] cc;
	private Address[] bcc;
	private Long mailSendingTime;
    private int mailRefId;
    private String mailMsg;
	
	//private String[] attachements;
	/*
	time
	source
	*/
	public EmailDTO()
	{
		
	}	
	
	
	public String getFromEmail() {
		return fromEmail;
	}


	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}


	public String getToEmail() {
		return toEmail;
	}


	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public Address[] getCc() {
		return cc;
	}


	public void setCc(Address[] addresses) {
		this.cc = addresses;
	}


	public Address[] getBcc() {
		return bcc;
	}


	public void setBcc(Address[] bcc) {
		this.bcc = bcc;
	}


	
	public Long getMailSendingTime() {
		return mailSendingTime;
	}



	public void setMailSendingTime(Long mailSendingTime) {
		this.mailSendingTime = mailSendingTime;
	}
	
	public String getDomainName() {
		return domainName;
	}



	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	

	public int getMailRefId() {
		return mailRefId;
	}


	public void setMaiReflId(int msgNumber) {
		this.mailRefId = msgNumber;
	}

	public String getMailMsg() {
		return mailMsg;
	}



	public void setMailMsg(String mailMsg) {
		this.mailMsg = mailMsg;
	}





		

}
