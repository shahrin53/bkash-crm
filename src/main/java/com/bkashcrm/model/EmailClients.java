package com.bkashcrm.model;

import javax.persistence.*;

@Entity
@Table(name="support_email_clients")

public class EmailClients{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="email")
	private String email;
	
	@Column(name="added_on")
	private long added_on;

	public long getAdded_on() {
		return added_on;
	}

	public void setAdded_on(long added_on) {
		this.added_on = added_on;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}

