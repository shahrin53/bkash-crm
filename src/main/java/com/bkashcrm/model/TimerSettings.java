package com.bkashcrm.model;


import javax.persistence.*;

@Entity
@Table(name="timer_settings")

public class TimerSettings{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="module")
	private String module;

	@Column(name="parameter")
	private String parameter;
	
	@Column(name="param_value")
	private int param_value;
	
	@Column(name="unit")
	private String unit;


	public String getModule() {
		return module;
	}

	public void setModule(String moduleName) {
		this.module = moduleName;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameterName) {
		this.parameter = parameterName;
	}

	public int getParamValue() {
		return param_value;
	}

	public void setParamValue(int value) {
		this.param_value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "TimerSettings [id=" + id + ", module=" + module + ", parameter=" + parameter + ", param_value="
				+ param_value + ", unit=" + unit + "]";
	}

	
}
