package com.bkashcrm.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bkashcrm.model.CommonAttributes;
import com.bkashcrm.util.HibernateUtil;


public class EmailAttachmentAllowedExtRepository {
	final static Logger logger = Logger.getLogger(EmailAttachmentAllowedExtRepository.class);
	private static EmailAttachmentAllowedExtRepository emailAttachmentAllowedExtRepository;
	private ArrayList<String> fileExtList=null;
	
	private EmailAttachmentAllowedExtRepository() {
		reload(); 
	}
	
	public static EmailAttachmentAllowedExtRepository getInstance() {
		createFileAttachmentExtRepository();
		return emailAttachmentAllowedExtRepository;
	}
	
	private synchronized static void createFileAttachmentExtRepository() {
		if (emailAttachmentAllowedExtRepository == null)
			emailAttachmentAllowedExtRepository = new EmailAttachmentAllowedExtRepository();
	}
	
	private void  reload() {
		
        List<String> settings = null;
        Session session=null;
        try {
            SessionFactory sessionFactory = HibernateUtil.sessionFactory;
            session = sessionFactory.openSession();
            Transaction tx = null;
            tx = session.beginTransaction();
            String hql = "SELECT attribute_name From CommonAttributes where module_name=:module";
            Query query = session.createQuery(hql);
            query.setParameter("module", "email_attach_ext");
            
            settings = query.list();
            tx.commit();
            
            fileExtList = new ArrayList<String>();
            
            for(int i=0;i<settings.size();i++)
            {
            	fileExtList.add(settings.get(i));
            }

        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        	if(session!=null)
        	session.close();
         //   return settings;
        }
 
    }
	
	public synchronized ArrayList<String> getAllFileExtensions() {
		return fileExtList;
	}
}
