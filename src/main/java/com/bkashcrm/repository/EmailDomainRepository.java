package com.bkashcrm.repository;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bkashcrm.model.EmailDomain;
import com.bkashcrm.model.TimerSettings;
import com.bkashcrm.util.HibernateUtil;


public class EmailDomainRepository {
	final static Logger logger = Logger.getLogger(EmailDomainRepository.class);
	private static EmailDomainRepository emailDomainRepository;
	private List<EmailDomain> mailDomainList=null;	
	private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	private EmailDomainRepository() {
		reload(); 
	}
	
	public static EmailDomainRepository getInstance() {
		createEmailDomainRepository();
		return emailDomainRepository;
	}
	
	private synchronized static void createEmailDomainRepository() {
		if (emailDomainRepository == null)
			emailDomainRepository = new EmailDomainRepository();
	}
	
	private void  reload() {
		
        List<EmailDomain> settings = null;
        Session session=null;
        
        try {
            SessionFactory sessionFactory = HibernateUtil.sessionFactory;
            session = sessionFactory.openSession();
            Transaction tx = null;
            tx = session.beginTransaction();
            String hql = "From EmailDomain";
            Query query = session.createQuery(hql);
            
            settings = query.list();
            tx.commit();
            
            mailDomainList = new ArrayList();
            mailDomainList.addAll(settings);
           
        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        	if(session!=null)
        	session.close();
         //   return settings;
        }
 
    }
	
	public synchronized List getSupportEmailDomain() {
		return mailDomainList;
	}
	
	public HashMap<String,Integer> getDomainIdByName(ArrayList<String> domainUsers)
	{
		HashMap<String,Integer> domainNameToIdMap = new HashMap<String,Integer>();
		Session session=null;
		String hql;
	    Query query;
	    List<Object[]> results=null;
		
		try {
        	SessionFactory sessionFactory = HibernateUtil.sessionFactory;
            session = sessionFactory.openSession();
            
            hql = "SELECT id,user From EmailDomain where user IN (:domainUsers)";
            query = session.createQuery(hql);
            query.setParameterList("domainUsers", domainUsers);
            results = query.list();
         
            
            for(Object obj[] : results){
            	domainNameToIdMap.put((String)obj[1], (Integer)obj[0]);
            }		
           
            
        }
		
		catch(Exception e) {
        	
        	System.out.println(e);
        	logger.debug(e);
        	
        }
        finally {
        	session.close();
        }
		
		return domainNameToIdMap;
	}
}
