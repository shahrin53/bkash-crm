package com.bkashcrm.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.bkashcrm.dto.EmailAttachmentDTO;
import com.bkashcrm.dto.EmailDTO;
import com.bkashcrm.model.EmailDomain;
import com.bkashcrm.model.EmailInfo;
import com.bkashcrm.repository.BlacklistedClientEmailRepository;
import com.bkashcrm.repository.EmailAttachmentAllowedExtRepository;
import com.bkashcrm.repository.EmailDomainRepository;
import com.bkashcrm.util.AppConstant;



import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;
import javax.mail.Folder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class EmailReceiverDAO 
{
	static Logger logger=Logger.getLogger(EmailReceiverDAO.class.getName());
	
	public Folder inboxFolder;
	public Message[] msg = null;
	String msgBody,from,fileExt,fname;

	int msgNumber;
	List<File> attachments = new ArrayList<File>();
	//String[] fname;
	public boolean textIsHtml = false;
	
	private SessionFactory sessionFactory;
	String hql;
	Query query;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
	
	public void getUnreadEmail()
	{
		/*
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");		
		EmailReceiverDAO emRcvDAO = context.getBean(EmailReceiverDAO.class);
		*/
		
		try {
			Properties props = System.getProperties();
					
			javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, null);
					
			Store store = mailSession.getStore("imaps");
			
			List<EmailDomain> domainProperties = EmailDomainRepository.getInstance().getSupportEmailDomain();
			ArrayList<String> blackListedClients = BlacklistedClientEmailRepository.getInstance().getAllBlacklistedEmails();
			ArrayList<String> allowedfileExtions = EmailAttachmentAllowedExtRepository.getInstance().getAllFileExtensions();
			ArrayList<EmailDTO> newEmailList = new ArrayList<EmailDTO>();
			HashMap<String,String> attchFilesNameMap = new HashMap<String,String>();
			ArrayList<EmailAttachmentDTO> mailAttchFileList = new ArrayList<EmailAttachmentDTO>();
			HashMap<Integer,ArrayList<EmailAttachmentDTO>> mailAttchFilesMap = new HashMap<Integer,ArrayList<EmailAttachmentDTO>>();
			EmailDTO mailDto =null;
			EmailAttachmentDTO mailAttachmntDto =null;
			
			//EmailInfo mailinfo = new EmailInfo();
			
			
			if(domainProperties!=null)
			{
				for(EmailDomain prop : domainProperties)
				{
					store.connect(prop.getDomainName(),prop.getSmtp_incoming_port(),prop.getUser(),prop.getPassword());
					
					inboxFolder = store.getFolder("INBOX");
					inboxFolder.open(Folder.READ_WRITE);							 
				    
					msg = inboxFolder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
		            int unreadMailCount = msg.length;		           
		            System.out.println("No. of Unread Messages : " + unreadMailCount);
					
					// if no message found then exit
					if(unreadMailCount==0) System.exit(0);
					
					for (int i = 0; i < msg.length; i++)
		            {
						mailDto = new EmailDTO();
						mailDto.setDomainName(prop.getDomainName());
						mailDto.setToEmail(prop.getUser());
						
						from = InternetAddress.toString(msg[i].getFrom());						
						
						if(from.indexOf("<")>0) // get the email address only
                    	{
                    		from=from.substring(from.indexOf("<")+1, from.indexOf(">"));                    		                    		
                    	}
						mailDto.setFromEmail(from);
						
						mailDto.setCc(msg[i].getRecipients(Message.RecipientType.CC));
						mailDto.setBcc(msg[i].getRecipients(Message.RecipientType.BCC)); // not work
						mailDto.setMailSendingTime(msg[i].getReceivedDate().getTime());
						
						msgNumber = msg[i].getMessageNumber();
			            mailDto.setMaiReflId(msgNumber);
						
						//System.out.println("from: "+from);
		                
		                if(blackListedClients.contains(from))
		                {
		                	logger.debug("Blacklisted Email : "+from);
		                	msg[i].setFlag(Flags.Flag.SEEN,true);
		                	continue;
		                }
		                //System.out.println("from: "+from);
		                System.out.println("Mail Subject "+msg[i].getSubject()+" From : "+from);
		                mailDto.setSubject(msg[i].getSubject());
		                
		                try
		                {
		                    Multipart part = (Multipart) msg[i].getContent();
		                    String contentType = msg[i].getContentType();
		                    BodyPart bodyPart = part.getBodyPart(0);
		                    msgBody = getText(bodyPart).toString();
		                    mailDto.setMailMsg(msgBody);
		                    
		                    
		                    // check for file attachment
		                    
		                    
		                  /*  if( !Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())
                                    //&& !StringUtils.isNotBlank(bodyPart.getFileName()))
		                    if(bodyPart.getDisposition() != null && bodyPart.getDisposition().equalsIgnoreCase(Part.ATTACHMENT))
                            {
   
                                continue; //dealing with attachments only
                            }
                            */                 
		                   
		                    
		                    if (contentType.contains("multipart")) {
		                        // content may contain attachments		                        
		                        int numberOfParts = part.getCount();
		                        ArrayList<MimeBodyPart> mimepartList = new ArrayList<MimeBodyPart>();
		                        
		                        for (int partCount = 0; partCount < numberOfParts; partCount++) {
		                            MimeBodyPart mimepart = (MimeBodyPart) part.getBodyPart(partCount);		                            
		                            if (Part.ATTACHMENT.equalsIgnoreCase(mimepart.getDisposition())) {
		                                // this part is attachment
		                            	mimepartList.add(mimepart);	                            	
		                                
		                            } 
		                        }
		                        
		                        attchFilesNameMap=getAttachedFiles(mimepartList,allowedfileExtions);
		                        
		                        if(attchFilesNameMap.size()>0)
		                        {
		                        	Iterator<String> fileItr = attchFilesNameMap.keySet().iterator();
		                        	String fileStoredName =null;
		                        	mailAttchFileList = new ArrayList<EmailAttachmentDTO>();
		                        	
		                        	 while(fileItr.hasNext())
		                        	 {
		                        		 mailAttachmntDto = new EmailAttachmentDTO();
		                        		 
		                        		 fileStoredName = fileItr.next();
		                        		 mailAttachmntDto.setStoredFileName(fileStoredName);
		                        		 mailAttachmntDto.setOriginalFileName(attchFilesNameMap.get(fileStoredName));
		                        		 mailAttachmntDto.setMaiReflId(mailDto.getMailRefId());
		                        		 mailAttchFileList.add(mailAttachmntDto);
		                        	 }
		                        	 
		                        	 mailAttchFilesMap.put(mailDto.getMailRefId(), mailAttchFileList);
		                        }
		     
		                        
		                    }
		                    
		                    
		                  
		                }
		                catch (Exception ex)
		                {
		                	System.out.println(ex);
		                	msgBody = msg[i].getContent().toString();
		                }
		       
		               msgBody = br2nl(msgBody);
		               msgBody = msgBody.replaceAll("\\&.*?\\;", "");
		               msgBody=msgBody.replace("\n", "<br>"); 
		               
		               
		               
		               newEmailList.add(mailDto);
		            }
					
					 try
						
		               {
		
		                   printAllMessages(msg);
		
		                   inboxFolder.close(true);
		                   store.close();
		
		               }
		
		               catch (Exception ex)
		
		               {
		                   System.out.println("Exception arise at the time of read mail");
		
		                   ex.printStackTrace();
		
		               }
					
					   new SaveEmailDAO().setEmailInfo(newEmailList,mailAttchFilesMap);
				}
            }
			
		}
		catch (Exception e) {
			System.out.println(e);
			logger.fatal("Exception", e);
		} finally {
			/*
			if(processedHeaderValue.length>1 && processedHeaderValue[1]!=null && processedHeaderValue[1].equals("")==false){
			dbProcess(emailMap);
			*/
			
			}
			
	 }
	public void printAllMessages(Message[] msgs) throws Exception
    {
        for (int i = 0; i < msgs.length; i++)
        {

            System.out.println("MESSAGE #" + (i + 1) + ":");

            printEnvelope(msgs[i]);
        }

    }

    public void printEnvelope(Message message) throws Exception

    {

        Address[] a;

        // FROM

        if ((a = message.getFrom()) != null) {
            for (int j = 0; j < a.length; j++) {
                System.out.println("FROM: " + a[j].toString());
            }
        }
        // TO
        if ((a = message.getRecipients(Message.RecipientType.TO)) != null) {
            for (int j = 0; j < a.length; j++) {
                System.out.println("TO: " + a[j].toString());
            }
        }
        String subject = message.getSubject();

        Date receivedDate = message.getReceivedDate();
        Date sentDate = message.getSentDate(); // receivedDate is returning
                                                // null. So used getSentDate()

        String content = message.getContent().toString();
        System.out.println("Subject : " + subject);
        if (receivedDate != null) {
            System.out.println("Received Date : " + receivedDate.toString());
        }
        System.out.println("Sent Date : " + sentDate.toString());
        System.out.println("Content : " + content);

       // getContent(message);

    }
    public HashMap<String,String> getAttachedFiles(ArrayList<MimeBodyPart> pList,ArrayList<String> extList) throws MessagingException, IOException 
    {
    	HashMap<String,String> fileNameMap = new HashMap<String,String>();
    	
    	try
    	{
	    	long currentTime=System.currentTimeMillis();
	    	String tempStr =null;
	    	MimeBodyPart p=null;
	    	
	    	for(int j=0;j<pList.size();j++)
	    	{
	    		p = pList.get(j);
		    	tempStr = p.getFileName().toString();         
		         	// check file extension
		     	//boolean allowedExt=false;
		     	
		     	fileExt = tempStr.substring(tempStr.lastIndexOf(".")+1);
		         
		     	/*
		         for (String tempExtension : extensionList) {
		     		if(fext.equalsIgnoreCase(tempExtension)) {
		     			allowedExt=true;	
		     		}
		     	}
		     	*/
		         
		         for(int i=0;i<extList.size();i++)
		         {
		        	 if(fileExt.equalsIgnoreCase(extList.get(i)))
		        	 {
		        		 //allowedExt=true;
		        		 fname=currentTime+"."+fileExt;
		        		 InputStream is = p.getInputStream();
		                 File f = new File(AppConstant.file_store_location);
		                 
		                 p.saveFile(AppConstant.file_store_location + File.separator + fname);
		                 
		                 /*
		                 FileOutputStream fos = new FileOutputStream(f);
		                 byte[] buf = new byte[4096];
		                 int bytesRead;
		                 while ((bytesRead = is.read(buf))!=-1)
		                 {
		                     fos.write(buf, 0, bytesRead);
		                 }
		                 fos.close();*/
		                 attachments.add(f);
		                 fileNameMap.put(fname, tempStr);
		        	 }
		         }
	         }
	         
	         /*
	         if(allowedExt)
	           fname=currentTime+fileExt;
	         else
	         	continue;
	         
	         InputStream is = p.getInputStream();
	         File f = new File("/usr/local/apache-tomcat-7.0.59/webapps/ROOT/res/"+fname);
	         FileOutputStream fos = new FileOutputStream(f);
	         byte[] buf = new byte[4096];
	         int bytesRead;
	         while ((bytesRead = is.read(buf))!=-1)
	         {
	             fos.write(buf, 0, bytesRead);
	         }
	         fos.close();
	         attachments.add(f);
	          
	         int t = multipart.getCount();
	         t--;         
	         k++;
	         */
    	}
    	catch(Exception e)
    	{
    		logger.debug(e);
    	}
    	
    	return fileNameMap;
    	
    }
    /*
    public void getContent(Message msg)

    {
        try {
            String contentType = msg.getContentType();
            System.out.println("Content Type : " + contentType);
            //Multipart mp = (Multipart) msg.getContent();
            int count=0;
            Object content = msg.getContent();  
            
            if (content instanceof String)  
            {  
                String body = (String)content;  
                //count = body.getCount();
               
                    dumpPart(body.pa);
                
            }  
            else if (content instanceof Multipart)  
            {  
                Multipart mp = (Multipart)content;  
                count = mp.getCount();
                for (int i = 0; i < count; i++) {
                    dumpPart(mp.getBodyPart(i));
                }
            }  
            
            
        } catch (Exception ex) {
            System.out.println("Exception arise at get Content");
            ex.printStackTrace();
        }
    }

    public void dumpPart(Part p) throws Exception {
        // Dump input stream ..
        InputStream is = p.getInputStream();
        // If "is" is not already buffered, wrap a BufferedInputStream
        // around it.
        if (!(is instanceof BufferedInputStream)) {
            is = new BufferedInputStream(is);
        }
        int c;
        System.out.println("Message : ");
        while ((c = is.read()) != -1) {
            System.out.write(c);
        }
    }
    */
	public String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String) p.getContent();
			textIsHtml = p.isMimeType("text/html");
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					if (text == null)
						text = getText(bp);
					continue;
				} else if (bp.isMimeType("text/html")) {
					String s = getText(bp);
					if (s != null)
						return s;
				} else {
					return getText(bp);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}

		return null;
	}
	public static String br2nl(String html) {
	        if(html==null)return html;
	        Document document = Jsoup.parse(html);
	        document.outputSettings(new Document.OutputSettings().prettyPrint(false));//makes html() preserve linebreaks and spacing
	        document.select("br").append("\\n");
	        document.select("p").prepend("\\n\\n");
	        String s = document.html().replaceAll("\\\\n", "\n");
	        return Jsoup.clean(s, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
	}
	/* 
	public List<EmailDomain> getEmailConnectors() {
		// TODO Auto-generated method stub
		Session session =null;
		List<EmailDomain> connectorsList = new ArrayList<EmailDomain>();
		
		try
		{
			session = this.sessionFactory.openSession();
			
			hql = "From EmailDomain WHERE "
					+ " delete_status=:dlt_status";
			query = session.createQuery(hql);
			query.setParameter("dlt_status", 0);
			
			connectorsList = query.list();
			
			//session.close();
		}
		catch (Exception e) {
			System.out.println(e);
            e.printStackTrace();
        } finally {
        	session.close();
           
        }
		
		return connectorsList;
	}
	*/				
					
	
}