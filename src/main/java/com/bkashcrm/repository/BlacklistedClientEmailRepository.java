package com.bkashcrm.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bkashcrm.model.BlacklistedEmails;
import com.bkashcrm.util.HibernateUtil;


public class BlacklistedClientEmailRepository {
	final static Logger logger = Logger.getLogger(BlacklistedClientEmailRepository.class);
	private static BlacklistedClientEmailRepository blackClientEmailRepository;
	private ArrayList<String> emailList=null;
	
	private BlacklistedClientEmailRepository() {
		reload(); 
	}
	
	public static BlacklistedClientEmailRepository getInstance() {
		creatblackClientEmailRepository();
		return blackClientEmailRepository;
	}
	
	private synchronized static void creatblackClientEmailRepository() {
		if (blackClientEmailRepository == null)
			blackClientEmailRepository = new BlacklistedClientEmailRepository();
	}
	
	private void  reload() {
		
        List<String> settings = null;
        Session session=null;
        try {
            SessionFactory sessionFactory = HibernateUtil.sessionFactory;
            session = sessionFactory.openSession();
            Transaction tx = null;
            tx = session.beginTransaction();
            String hql = "SELECT email From BlacklistedEmails";
            Query query = session.createQuery(hql);
            
            settings = query.list();
            tx.commit();
            
            emailList = new ArrayList<String>();
            
            for(int i=0;i<settings.size();i++)
            {
            	emailList.add(settings.get(i));
            }

        } catch (Exception e) {
        	e.printStackTrace();
        } finally {
        	if(session!=null)
        	session.close();
         //   return settings;
        }
 
    }
	
	public synchronized ArrayList<String> getAllBlacklistedEmails() {
		return emailList;
	}
}
