package com.bkashcrm.model;


import javax.persistence.*;

@Entity
@Table(name="email_domain")

public class EmailDomain{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;
	
	@Column(name="domain_name")
	private String domainName;

	@Column(name="IP")
	private String IP;
	
	@Column(name="user")
	private String user;
	
	@Column(name="password")
	private String password;
	
	@Column(name="smtp_incoming_port")
	private int smtp_incoming_port;
	
	@Column(name="smtp_outgoing_port")
	private int smtp_outgoing_port;
	
	@Column(name="tls")
	private String tls;

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getSmtp_incoming_port() {
		return smtp_incoming_port;
	}

	public void setSmtp_incoming_port(int smtp_incoming_port) {
		this.smtp_incoming_port = smtp_incoming_port;
	}

	public int getSmtp_outgoing_port() {
		return smtp_outgoing_port;
	}

	public void setSmtp_outgoing_port(int smtp_outgoing_port) {
		this.smtp_outgoing_port = smtp_outgoing_port;
	}

	public String getTls() {
		return tls;
	}

	public void setTls(String tls) {
		this.tls = tls;
	}
	
}