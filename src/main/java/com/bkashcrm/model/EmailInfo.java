package com.bkashcrm.model;


import javax.persistence.*;

@Entity
@Table(name="support_email")

public class EmailInfo{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mail_id;	

	@Column(name="client_id")
	private int client_id;
	
	@Column(name="agent_id")
	private int agent_id;
	
	@Column(name="mail_cc")
	private String mail_cc;
	
	@Column(name="mail_bcc")
	private String mail_bcc;
	
	@Column(name="mail_subject")
	private String mail_subject;
	
	@Column(name="mail_creation_time")
	private long mail_creation_time;
	
	@Column(name="mail_reply_time")
	private long mail_reply_time;
	
	@Column(name="category_id")
	private int category_id;
	
	@Column(name="subcategory_id")
	private int subcategory_id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="status")
	private String status;
	
	@Column(name="tags")
	private String tags;
	
	@Column(name="source")
	private String source;

	public int getMail_id() {
		return mail_id;
	}

	public void setMail_id(int mail_id) {
		this.mail_id = mail_id;
	}
	
	@Column(name="domain_id")
	private int domain_id;

	public int getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(int domain_id) {
		this.domain_id = domain_id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

	public int getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}

	public String getMail_cc() {
		return mail_cc;
	}

	public void setMail_cc(String mail_cc) {
		this.mail_cc = mail_cc;
	}

	public String getMail_bcc() {
		return mail_bcc;
	}

	public void setMail_bcc(String mail_bcc) {
		this.mail_bcc = mail_bcc;
	}

	public String getMail_subject() {
		return mail_subject;
	}

	public void setMail_subject(String mail_subject) {
		this.mail_subject = mail_subject;
	}

	public long getMail_creation_time() {
		return mail_creation_time;
	}

	public void setMail_creation_time(long mail_creation_time) {
		this.mail_creation_time = mail_creation_time;
	}

	public long getMail_reply_time() {
		return mail_reply_time;
	}

	public void setMail_reply_time(long mail_reply_time) {
		this.mail_reply_time = mail_reply_time;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
}