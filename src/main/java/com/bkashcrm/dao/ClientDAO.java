package com.bkashcrm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.bkashcrm.util.HibernateUtil;


public class ClientDAO
{
   final static Logger logger = Logger.getLogger(ClientDAO.class);

   public HashMap<String,Integer> getClientIdbyEmail(ArrayList<String> clientEmails) 
   {	

	HashMap<String,Integer> clientEmailToIdMap = new HashMap<String,Integer>();
	Session session=null;
	String hql;
    Query query;
    List<Object[]> results=null;
	
	try {
		
    	SessionFactory sessionFactory = HibernateUtil.sessionFactory;
    	session = sessionFactory.openSession();
        
        hql = "SELECT id,email From EmailClients where email IN (:clientEmails)";
        query = session.createQuery(hql);
        query.setParameterList("clientEmails", clientEmails);
        results = query.list();
             
        for(Object obj[] : results){
        	clientEmailToIdMap.put((String)obj[1], (Integer)obj[0]);
        }
        
        if(clientEmails.size()>clientEmailToIdMap.size())
        {
        	ArrayList<String> newClientEmails = new ArrayList<String>();
        	
        	for(int i=0;i<clientEmails.size();i++)
        	{
        		if(clientEmailToIdMap.containsKey(clientEmails.get(i))==false)
        			newClientEmails.add(clientEmails.get(i));
        			
        	}
        	
        	setNewClient(newClientEmails) ;
        	
        	/********************************************/
        	clientEmailToIdMap.putAll(getClientIdbyEmail(newClientEmails));
        }
        
    }	
	catch(Exception e) {
    	
    	System.out.println(e);
    	logger.debug(e);
    	
    }
    finally {
    	session.close();
    }
	
	return clientEmailToIdMap;
		
	}
   

	public void setNewClient(ArrayList<String> clientEmails) {
		
		HashMap<String,Integer> clientEmailToIdMap = new HashMap<String,Integer>();
		String hql,sql,values="";
		Query query;
		Session session=null;
		int insertExcessStatus = 0;
		
		try {
	    	SessionFactory sessionFactory = HibernateUtil.sessionFactory;
	        session = sessionFactory.openSession();
	        session.beginTransaction();
	        Long currentTime =System.currentTimeMillis();
	        
	        /*
	        sql = "insert into support_email_clients (email, added_on) VALUES ";
	        
	        for(int i=0;i<clientEmails.size();i++)
	        {
	        	if(values.equals("")==false)
	        		values+=",";
	        	
	        	values+="('"+clientEmails.get(i)+"',"+currentTime+")";	        	
	        		
	        }
	        
	        sql += values;
	        */
	        for(int i=0;i<clientEmails.size();i++)
	        {
	        	try
	        	{
			        hql = "insert into support_email_clients (email, added_on) VALUES (:client_email,:add_time)";
							
					query = session.createNativeQuery(hql);
					query.setParameter("client_email", clientEmails.get(i));
					query.setParameter("add_time", currentTime);
					
					insertExcessStatus = query.executeUpdate();
	        	}
	        	  catch(Exception e) {
	              	
	              	System.out.println(e);
	              	logger.debug(e);
	              	
	              }
	        }
	        
	        /*
	        query = session.createQuery(sql);
	        query.executeUpdate();
	        */	       
	      
		}
        catch(Exception e) {
        	
        	System.out.println(e);
        	logger.debug(e);
        	session.getTransaction().rollback();
        	
        }
        finally {
        	session.getTransaction().commit();
        	session.close();
        }
	}
		
	
}