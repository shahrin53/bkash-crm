package com.bkashcrm.util;


import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;


public class TimeConversion {
	
	static Logger logger=Logger.getLogger(TimeConversion.class.getName());
	
	public static long TimeToLong(int dbtime,String unitType) {

		long milliseconds =0;
		
		try {
			
			if(unitType!=null && unitType.length()>0)
			{
				if(unitType.equals("seconds"))
					milliseconds = TimeUnit.SECONDS.toMillis(dbtime) ;
				else if(unitType.equals("minutes"))
					milliseconds = TimeUnit.SECONDS.toMillis(TimeUnit.MINUTES.toSeconds(dbtime));
				else if(unitType.equals("hours"))
					milliseconds = TimeUnit.SECONDS.toMillis(TimeUnit.HOURS.toSeconds(dbtime));
					
			}
		}
		catch (Exception ex) {
			logger.fatal("Exception",ex);
		}
		return milliseconds;
	}
	
}