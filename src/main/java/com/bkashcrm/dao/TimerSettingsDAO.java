package com.bkashcrm.dao;

import java.util.List;

import com.bkashcrm.model.TimerSettings;

public interface TimerSettingsDAO {	
	
	public List<TimerSettings> getParameters(String module);
	
}