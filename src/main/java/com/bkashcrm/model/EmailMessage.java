package com.bkashcrm.model;


import javax.persistence.*;

@Entity
@Table(name="support_email_msg")

public class EmailMessage{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="mail_id")
	private int mail_id;

	@Column(name="msg")
	private String parameter;
	
	@Column(name="msg_by")
	private String msg_by;
	
	@Column(name="msg_time")
	private long msg_time;

	public int getModule() {
		return mail_id;
	}

	public void setModule(int mail_id) {
		this.mail_id = mail_id;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getMsg_by() {
		return msg_by;
	}

	public void setMsg_by(String msg_by) {
		this.msg_by = msg_by;
	}

	public long getMsg_time() {
		return msg_time;
	}

	public void setMsg_time(long msg_time) {
		this.msg_time = msg_time;
	}

   
	
	
}
