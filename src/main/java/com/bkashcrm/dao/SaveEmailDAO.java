package com.bkashcrm.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.bkashcrm.dto.EmailAttachmentDTO;
import com.bkashcrm.dto.EmailDTO;
import com.bkashcrm.dto.EmailMsgDTO;
import com.bkashcrm.model.EmailInfo;
import com.bkashcrm.repository.EmailDomainRepository;
import com.bkashcrm.util.HibernateUtil;

public class SaveEmailDAO implements EmailStoreDAO {
	
	static final Logger logger = Logger.getLogger(SupportEmailSchedulerDAO.class.getName());
	private SessionFactory sessionFactory;	
	String hql;
	Query query;
	
	@Override
	public void setEmailInfo(ArrayList<EmailDTO> mailDtos,HashMap<Integer,ArrayList<EmailAttachmentDTO>> mailAttachments) {
		// TODO Auto-generated method stub
		
		Session session =null;
				
		try
		{
			ArrayList<String> domainUsers = new ArrayList<String>();
			ArrayList<String> clientEmails = new ArrayList<String>();
			EmailDTO mailDTO= null;
			String domainUserName,clientEmail;
			
			for(int i=0;i<mailDtos.size();i++)
			{
				mailDTO = mailDtos.get(i);
				domainUserName = mailDTO.getToEmail().trim();
				clientEmail = mailDTO.getFromEmail().trim();
				
				if(domainUserName!=null && domainUserName.length()>0 && domainUsers.contains(domainUserName)==false)
					domainUsers.add(domainUserName);
				
				if(clientEmail!=null && clientEmail.length()>0 && clientEmails.contains(clientEmail)==false)
					clientEmails.add(clientEmail);
					
			}
			
			HashMap<String,Integer> domainNameToIdMap = EmailDomainRepository.getInstance().getDomainIdByName(domainUsers);
			HashMap<String,Integer> clientEmailToIdMap = new ClientDAO().getClientIdbyEmail(clientEmails);			
			
			sessionFactory = HibernateUtil.sessionFactory;
			session = sessionFactory.openSession();
			session.beginTransaction();			
			Long currentTime =System.currentTimeMillis();
		    int primary_key_mailID;
		    EmailInfo emailObj;
			
			for(int i=0;i<mailDtos.size();i++)
	        {			
					mailDTO = mailDtos.get(i);
					emailObj = new EmailInfo();					
					
					emailObj.setDomain_id(domainNameToIdMap.get(mailDTO.getToEmail().trim()));
					emailObj.setClient_id(clientEmailToIdMap.get(mailDTO.getFromEmail().trim()));
					emailObj.setMail_cc(mailDTO.getCc()!=null? Arrays.toString(mailDTO.getCc()):"");
					emailObj.setMail_bcc(mailDTO.getBcc()!=null? Arrays.toString(mailDTO.getBcc()):"");
					emailObj.setMail_subject(mailDTO.getSubject());
					emailObj.setMail_creation_time(currentTime);
					emailObj.setStatus("new");
					emailObj.setSource("mail");
					
									
					primary_key_mailID = (Integer) session.save(emailObj);		
					System.out.println(primary_key_mailID);
					
					
					if(primary_key_mailID>0 && mailAttachments.containsKey(mailDTO.getMailRefId()))
					{
					     setMailAttachment(primary_key_mailID,mailAttachments.get(mailDTO.getMailRefId())); 
					}
						
					EmailMsgDTO mailMsgDto = new EmailMsgDTO();	
					mailMsgDto.setMailId(primary_key_mailID);
					mailMsgDto.setMailSendingTime(mailDTO.getMailSendingTime());
					mailMsgDto.setMsg((mailDTO.getMailMsg()!=null && mailDTO.getMailMsg().length()>0)?mailDTO.getMailMsg():"");
					
					setEmailMessage(mailMsgDto);
	        }	 
	        
		}
		catch (Exception e) {
			
			System.out.println(e);
            e.printStackTrace();
            session.getTransaction().rollback();
            
        } finally {
        	session.getTransaction().commit();
        	session.close();
           
        }		
		
	}
	
	public void setMailAttachment(int mail_id,ArrayList<EmailAttachmentDTO> attachmentList)
	{		
		String hql;
		Query query;
		Session session=null;
		
		
		try {
	    	SessionFactory sessionFactory = HibernateUtil.sessionFactory;
	        session = sessionFactory.openSession();
	        session.beginTransaction();	      
	        
	      
	        for(int i=0;i<attachmentList.size();i++)
	        {
	        	
			        hql = "insert into email_attachment (mail_id, attached_filename,atcched_filename_original) VALUES (:mail_id,:attached_filename,:atcched_filename_original)";
							
					query = session.createNativeQuery(hql);
					query.setParameter("mail_id", mail_id);
					query.setParameter("attached_filename", attachmentList.get(i).getStoredFileName());
					query.setParameter("atcched_filename_original", attachmentList.get(i).getOriginalFileName());
					
					query.executeUpdate();     	
	        	 
	        }	        
	      
	        //session.getTransaction().commit();
	      
		}
        catch(Exception e) {
        	
        	System.out.println(e);
        	logger.debug(e);
        	session.getTransaction().rollback();
        	
        }
        finally {
        	session.getTransaction().commit();
        	session.close();
        }
		
	}	
	
	
	@Override
	public void setEmailMessage(EmailMsgDTO mailMsgDto) {
		// TODO Auto-generated method stub				
		
		String hql;
		Query query;
		Session session=null;
		
		
		try {
			
	    	SessionFactory sessionFactory = HibernateUtil.sessionFactory;
	        session = sessionFactory.openSession();
	        session.beginTransaction(); 
	       
	        	
	        hql = "insert into support_email_msg (mail_id, msg,msg_by,msg_time) VALUES (:mail_id,:msg,:msg_by,:msg_time)";
					
			query = session.createNativeQuery(hql);
			query.setParameter("mail_id", mailMsgDto.getMailId());
			query.setParameter("msg", mailMsgDto.getMsg().trim());
			query.setParameter("msg_by", "client");
			query.setParameter("msg_time", mailMsgDto.getMailSendingTime());
			
			query.executeUpdate();     	
	        	 
	        	        
	      
	        //session.getTransaction().commit();
	      
		}
        catch(Exception e) {
        	
        	System.out.println(e);
        	logger.debug(e);
        	session.getTransaction().rollback();
        	
        }
        finally {
        	session.getTransaction().commit();
        	session.close();
        }
		
	}
	
	
	
}