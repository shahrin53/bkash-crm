package com.bkashcrm.main;

import java.util.List;

//import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bkashcrm.dao.SupportEmailSchedulerDAO;
import com.bkashcrm.model.TimerSettings;
import com.bkashcrm.util.AppConstant;


public class SupportEmailMain {

	public static void main(String[] args) {

		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");		
		//SupportEmailSchedulerDAO gsesDAO = context.getBean(SupportEmailSchedulerDAO.class);
		SupportEmailSchedulerDAO gsesDAO = new SupportEmailSchedulerDAO();
		List<TimerSettings> list = gsesDAO.getParameters(AppConstant.support_email_module);	
		System.out.println(list.get(0).toString());
		gsesDAO.scheduleJobToGetSupportMail(list);
		
		//close resources
		//context.close();	
	}
}